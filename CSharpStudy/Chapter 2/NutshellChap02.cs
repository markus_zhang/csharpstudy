using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpStudy
{
    public class UnitConvertor
    {
        int ratio;
        public UnitConvertor(int unitRatio)
        {
            ratio = unitRatio;
        }
        public void SetRatio(int newRatio)
        {
            ratio = newRatio;
        }
        public int GetRatio()
        {
            return ratio;
        }
        private int Convert(int unit)
        {
            return unit * ratio;
        }
    }
    public class Test
    {
        static void Foo(StringBuilder fooSB)
        {
            fooSB.Append("test");
            fooSB = null;
        }

        static void Foo2(String str2)
        {
            str2 = "Foo2";   // This second "str is a copy of value of the parameter "str""
            str2 = "Foo3";
            str2 = "Foo4";
            Console.WriteLine(str2);
        }

        static void OutModifier(out string blah)    // out modifier, must assign value to blah before return
        {
            blah = "";
            return;
        }

        static void RefPassByValue()
        {
            StringBuilder ptr_sb = new StringBuilder();
            Foo(ptr_sb);
            Console.WriteLine(ptr_sb);
            String str = "Not test";
            Foo2(str);
            Console.WriteLine(str);
        }

        static void DiffTypeParams(int x, string y = "", params int[] intParams)
        {
            Console.WriteLine(x + y);
        }

        static void ExcelTest()
        {
            
        }

        static void Main(string[] args)
        {
            UnitConvertor blah1 = new UnitConvertor(100);
            UnitConvertor blah2 = blah1;
            blah1.SetRatio(10000);
            Console.WriteLine(blah1.GetRatio());
            Console.WriteLine(blah2.GetRatio());
            string s = $"255 in hex is {byte.MaxValue:X2}";
            UnitConvertor[] arr = new UnitConvertor[100];
            // Console.WriteLine(arr[0].GetRatio());   // Default value not available for UnitConvertor
            RefPassByValue();

            //  Parameter types
            DiffTypeParams(10, "I'm a string", new int[] { 0, 1, 2, 3});
            DiffTypeParams(y : "I'm a String", intParams : new int[] { 0, 3, 4, 5 }, x : 100);
        }
    }
}
