﻿#### **string** and **StringBuilder**

OK so what I get is that value type does not need and cannot use `new` while reference type can.

`string` is fucking compplicated, it is a reference type, and it is immutable, which means that you can use a `new` to initiate a `string`, but once you create one you cannot change the content.

Also, you cannot do this, as in C++:

```C#
string blah = new string("foo");
```
Because the constructor doesn't support this.

It is immutable, which means you get different copies of `blah` in the following code:

```C#
string blah = "Yes";
string blah = "Yes2";
```

Basically the first `blah` lost reference and would be removed from memory by gc after the second line. This is pretty tricky as it seems to show that `string` is immutable but actually it is not.

If you really want to change the content of the same string, either pass by ref or use `StringBuilder` instead:

```C#
static void MutateString(ref string blah)
{
  blah = "Mutated";
}

static void MutateString2(StringBuilder blah)
{
  blah = "Mutated";
}
```

Ok the second one works without `ref` because `StringBuilder` is a reference type and it's mutable.

#### Pass by value and Pass by reference

We also need to consider the case of value type and reference type, as well as the `string` class as an immutable reference type.

In large, I think it's very similar to what we have in C++, i.e. 
1) For value types, if we do not have `ref` or `out`(essesntially another by reference) then it is passed by value.

2) For reference types, without `ref` or `out`, it is also passed by value. However, reference type in C# is similar to C++ reference, i.e. they point to real data. So even if passed by value, the data itself can be changed. The only exception is `string` as it is immutable.

3) If `ref` or `out` are used, then it's passed by reference. `out` forces assignment of value before `return` and that's it.

#### Order of parameters and Named Arguments

Easy. First we have the mandatory ones, then the optional ones, and `params` is always the last.

Here is an example:

```C#
void Foo(int x, string y = "", params int[] intParams)
{
}
```

For languages like C++, we can only refer to the parameters by position, but in VBA as well as in C# it's possible to refer them by name, which is called **Named Arguments**. The order doesn't matter if we use them, because they are named so the compiler knows which one is which:

```C#
static void DiffTypeParams(int x, string y = "", params int[] intParams)
{
  Console.WriteLine(x + y);
}
//	Call in Main(), both are legal
DiffTypeParams(10, "I'm a string", new int[] { 0, 1, 2, 3});
DiffTypeParams(y : "I'm a String", intParams : new int[] { 0, 3, 4, 5 }, x : 100);
```